# packages loaded by default
float
caption
subcaption
graphicx        # can be disabled using package option nographic
pgfkeys
etoolbox
environ
array           # can be disabled using package option noarray
booktabs        # can be disabled using package option nobooktabs

# packages loaded if the corresponding package option is given
soft longtable
soft graphbox
