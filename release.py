#!/usr/bin/env python3

# Copyright © 2024 E. Zöllner
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See test/license.txt for more details.

import re
import os
import sys
import shutil
import datetime
import tempfile
import subprocess


# ------- settings -------

root = os.path.split(__file__)[0]
os.chdir(root)

package_name = next(os.path.splitext(fn)[0] for fn in os.listdir(root) if os.path.splitext(fn)[1] == '.ins')
fn_file_list = '%s-file-list.txt' % package_name
fn_license = 'license.txt'
fn_dtx = '%s.dtx' % package_name
fn_sty = '%s.dtx' % package_name
fn_doc_meta = 'doc/%s.tex' % package_name

# CTAN package checker https://www.ctan.org/pkg/pkgcheck, this script can install it from CTAN automatically
pkgcheck = shutil.which('pkgcheck') or os.path.expanduser('~/.local/utils/pkgcheck')

#: Files contained in this list will not be included in the archive.
#: Syntax see doc string of class FilePattern.
blacklist = [
	# don't include hidden files (.gitignore and .gitattributes)
	'.*',
	# "The package should not contain files which can be generated from other files in your package – beside the documentation. [...]
	#  For LaTeX packages which contain a dtx file the files which are generated from this dtx file must not be contained."
	# https://www.ctan.org/help/upload-pkg
	'*.sty',
	# don't include this file
	'./release.py',
	# don't include the tests
	'./test/**',
	# legacy file, contains nothing more than a link to the pdf anymore
	'./doc/*.md',
]

#: How to sort the files in the file list.
#: Syntax see doc string of class FilePattern.
sortlist = [
	'./*-file-list.txt',

	'./*.ins',
	'./*.dtx',
	'./*.sty',
	'./README.md',
	'./DEPENDS.txt',

	'./doc/%s.tex' % package_name,
	'./doc/*.pdf',
	'./doc/*.bib',
	'./doc/*.dbx',
	'./doc/**',
	'./doc/lexer/**',
]


# ------- ui -------

def ask_yes_no(prompt: str) -> bool:
	prompt += (" [Yn] ")
	while True:
		a = input(prompt).lower()
		if a == 'y':
			return True
		elif a == 'n':
			return False
		elif not a:
			return True
		else:
			print("invalid input")


# ------- compile settings -------

class FilePattern:

	'''
	- * matches anything except a slash
	- ** matches anything including slashes
	- patterns not including a slash are relative paths (can match in any subdirectory)
	- patterns including a slash are absolute paths (relative to root)
	- ./ at the start can be used to make a path absolute
	'''

	super_wildcard = '**'
	normal_wildcard = '*'

	def __init__(self, pattern: str) -> None:
		self.raw_pattern = pattern
		if '/' not in pattern:
			start = '(^|.*%s)' % re.escape(os.path.sep)
		else:
			start = '^'
			if pattern.startswith('./'):
				pattern = pattern[2:]
			pattern = pattern.replace('/', os.path.sep)
		pattern = re.escape(pattern)
		pattern = pattern.replace(re.escape(self.super_wildcard), '{SUPER_WILDCARD}')
		pattern = pattern.replace(re.escape(self.normal_wildcard), '{NORMAL_WILDCARD}')
		pattern = pattern.replace('{NORMAL_WILDCARD}', '[^/]*')
		pattern = pattern.replace('{SUPER_WILDCARD}', '.*')
		pattern = start + pattern + '$'
		self.reo = re.compile(pattern)

	def matches(self, fn: str) -> bool:
		return bool(self.reo.match(fn))

	def specific(self) -> int:
		return len(self.raw_pattern) - self.raw_pattern.count(self.super_wildcard)*100_000 - self.raw_pattern.count(self.normal_wildcard)*10_000

compiled_blacklist = [FilePattern(fn) for fn in blacklist]
compiled_sortlist = [FilePattern(fn) for fn in sortlist]

def is_blacklisted(fn: str) -> bool:
	for pattern in compiled_blacklist:
		if pattern.matches(fn):
			return True
	return False

def sort_key(fn: str) -> 'tuple[int, str]':
	patterns = [p for p in compiled_sortlist if p.matches(fn)]
	if not patterns:
		i = len(compiled_sortlist)
	else:
		patterns.sort(key=lambda p: p.specific())
		i = compiled_sortlist.index(patterns[-1])

	path, fn = os.path.split(fn)
	fn = '00_' + fn

	return (i, os.path.join(path, fn))


# ------- get files -------

def get_tracked_files() -> 'list[str]':
	cmd = ['git', 'ls-files']
	p = subprocess.run(cmd, cwd=root, stdout=subprocess.PIPE, text=True, check=True)
	return p.stdout.splitlines()

def get_files_for_archive() -> 'list[str]':
	out = [fn for fn in get_tracked_files() if not is_blacklisted(fn)]
	out.sort(key=sort_key)
	return out


# ------- update file list -------

def update_file_list() -> None:
	with open(fn_file_list, 'rt') as f:
		content = f.read().splitlines()
	i0 = content.index('')
	i1 = [i for i in range(len(content)) if content[i] == ''][-2] + 1
	license_info = content[:i0]
	test_info = content[i1:]
	files = get_files_for_archive()
	files.remove(fn_license)
	i = 1
	files = files[:i] + [''] + files[i:]
	i = next(i for i, fn in enumerate(files) if fn.startswith('doc'))
	files = files[:i] + [''] + files[i:]
	content = license_info + [''] + files + [''] + test_info
	with open(fn_file_list, 'wt') as f:
		f.write('\n'.join(content) + '\n')


# ------- check version and date -------

reo_version = re.compile(r'\\ProvidesPackage\{[^}]+\}\[(?P<year>[0-9]+)/(?P<month>[0-9]+)/(?P<day>[0-9]+) (?P<version>[^]]+)\]')

class Date:

	def __init__(self, year: int, month: int, day: int) -> None:
		self.year = year
		self.month = month
		self.day = day

	def __eq__(self, other: object) -> bool:
		if not isinstance(other, Date):
			return NotImplemented
		return self.year == other.year and self.month == other.month and self.day == other.day

	def __str__(self) -> str:
		return '%04d-%02d-%02d' % (self.year, self.month, self.day)

class Version:

	reo = re.compile(r'v?(?P<major>[0-9]+)\.(?P<minor>[0-9]+)\.(?P<patch>[0-9]+)')

	def __init__(self, raw: str) -> None:
		self.raw = raw
		m = self.reo.match(raw)
		assert m, "invalid version number: %r" % raw
		self.major = int(m.group('major'))
		self.minor = int(m.group('minor'))
		self.patch = int(m.group('patch'))

	def __eq__(self, other: object) -> bool:
		if not isinstance(other, Version):
			return NotImplemented
		return self.raw == other.raw

	def __repr__(self) -> str:
		return '%s(%r)' % (type(self).__name__, self.raw)

	def __str__(self) -> str:
		return self.raw

	def is_successor_of(self, other: 'Version') -> bool:
		if self.patch > 0:
			return self.major == other.major and self.minor == other.minor and self.patch - 1 == other.patch
		elif self.minor > 0:
			return self.major == other.major and self.minor - 1 == other.minor
		elif self.major > 0:
			return self.major - 1 == other.major
		assert False


def get_last_tag() -> Version:
	cmd = ['git', 'tag', '--sort=version:refname']
	p = subprocess.run(cmd, cwd=root, stdout=subprocess.PIPE, text=True, check=True)
	tag = p.stdout.splitlines()[-1]
	if tag:
		return Version(tag)
	return Version('v0.0.0')

def get_tex_version(fn: str) -> Version:
	with open(fn, 'rt') as f:
		for ln in f:
			m = reo_version.match(ln)
			if m:
				return Version(m.group('version'))
	assert False, "failed to find version in %s" % fn

def get_tex_date(fn: str) -> Date:
	with open(fn, 'rt') as f:
		for ln in f:
			m = reo_version.match(ln)
			if m:
				return Date(
					year = int(m.group('year')),
					month = int(m.group('month')),
					day = int(m.group('day')),
				)
	assert False, "failed to find date in %s" % fn

def get_doc_version() -> Version:
	fn = fn_doc_meta
	reo_version = re.compile(r'\\version\{(?P<version>.*?)\}')
	with open(fn, 'rt') as f:
		for ln in f:
			m = reo_version.match(ln)
			if m:
				return Version(m.group('version'))
	assert False, "failed to find version in %s" % fn

def get_doc_date() -> Date:
	fn = fn_doc_meta
	reo_version = re.compile(r'\\date\{(?P<month>.+?)~(?P<day>[0-9]+), (?P<year>[0-9]+)\}')
	months = {
		'January'   : 1,
		'February'  : 2,
		'March'     : 3,
		'April'     : 4,
		'May'       : 5,
		'June'      : 6,
		'July'      : 7,
		'August'    : 8,
		'September' : 9,
		'October'   : 10,
		'November'  : 11,
		'December'  : 12,
	}
	with open(fn, 'rt') as f:
		for ln in f:
			m = reo_version.match(ln)
			if m:
				return Date(
					year = int(m.group('year')),
					month = months[m.group('month')],
					day = int(m.group('day')),
				)
	assert False, "failed to find date in %s" % fn

def get_today() -> Date:
	t = datetime.date.today()
	return Date(year=t.year, month=t.month, day=t.day)

def assert_version_and_date() -> None:
	version_dtx = get_tex_version(fn_dtx)
	version_sty = get_tex_version(fn_sty)
	version_doc = get_doc_version()
	last_tag = get_last_tag()

	out = True
	if not version_dtx == version_sty:
		print(f"ERROR: version numbers in dtx and sty files do not match (version_dtx={version_dtx}, version_sty={version_sty})")
		out = False
	if not version_dtx == version_doc:
		print(f"ERROR: version numbers in dtx file and documentation do not match (version_dtx={version_dtx}, version_doc={version_doc})")
		out = False
	if not version_dtx.is_successor_of(last_tag):
		print(f"ERROR: version number in dtx is not a successor of the last version (version_dtx={version_dtx}, last_tag={last_tag})")
		out = False

	date_dtx = get_tex_date(fn_dtx)
	date_sty = get_tex_date(fn_sty)
	date_doc = get_doc_date()
	date_today = get_today()

	if not date_dtx == date_sty:
		print(f"ERROR: dates in dtx and sty files do not match (date_dtx={date_dtx}, date_sty={date_sty})")
		out = False
	if not date_dtx == date_doc:
		print(f"ERROR: dates in dtx file and documentation do not match (date_dtx={date_dtx}, date_doc={date_doc})")
		out = False
	if not date_dtx == date_today:
		print(f"WARNING: date in dtx file is not today (date_dtx={date_dtx}, date_today={date_today})")
		if out and not ask_yes_no("Do you want to continue, anyway?"):
			out = False

	if not out:
		sys.exit(1)


# ------- check git -------

def assert_git_clean() -> None:
	cmd = ['git', 'status', '--porcelain=v1']
	p = subprocess.run(cmd, cwd=root, stdout=subprocess.PIPE, text=True, check=True)
	status = p.stdout.splitlines()
	untracked = any(ln.startswith('??') for ln in status)
	dirty = any(not ln.startswith('??') for ln in status)
	if dirty:
		print("The repository is not clean.")
		print("Please commit or stash the changes before continuing.")
		print("")
		subprocess.run(['git','status'], cwd=root)
		sys.exit(1)
	if untracked:
		print("There are untracked files:")
		for ln in status:
			print("- %s" % ln[3:])
		if not ask_yes_no("Do you want to continue, anyway?"):
			sys.exit(1)


# ------- run tests -------

def run_tests() -> None:
	cmd = ['python3', 'test/autotest.py']
	p = subprocess.run(cmd, cwd=root)
	if p.returncode != 0:
		print("ERROR: Tests have failed")
		sys.exit(1)


# ------- build archive -------

def build_archive() -> None:
	files = get_files_for_archive()
	fn_archive = package_name + '.tar.gz'
	cmd = ['tar', '--transform', r's:^:%s/:' % package_name, '-czf', fn_archive, *files]
	subprocess.run(cmd, cwd=root, check=True)

	print("created %s" % fn_archive)
	test_archive(fn_archive)

	print("Go to the package specific page on CTAN and click on Upload")
	print("https://www.ctan.org/pkg/%s" % package_name)

def test_archive(fn_archive: str) -> None:
	if not os.path.exists(pkgcheck):
		print("ERROR: pkgcheck is not installed")
		if ask_yes_no("Do you want me to install it now to %s?" % pkgcheck):
			install_pkgcheck()
		else:
			sys.exit(1)

	with tempfile.TemporaryDirectory() as tmp:
		assert os.listdir(tmp) == []
		shutil.copy2(fn_archive, tmp)
		assert os.listdir(tmp) == [fn_archive]
		subprocess.run(['tar', '-xzf', fn_archive], cwd=tmp, check=True)
		tmp_files = os.listdir(tmp)
		if set(tmp_files) != {fn_archive, package_name}:
			print("ERROR: temporary directory does not have expected content after extracting the archive.")
			print("expected: the archive %r and the extracted directory %r" % (fn_archive, package_name))
			print("real content:")
			for fn in tmp_files:
				print("- %s" % fn)
			sys.exit(1)
		p = subprocess.run([pkgcheck, '-d', package_name], cwd=tmp)
		if p.returncode != 0:
			print("ERROR: pkgcheck failed")
			sys.exit(p.returncode)

def install_pkgcheck() -> None:
	url = "https://mirrors.ctan.org/support/pkgcheck.zip"
	with tempfile.TemporaryDirectory() as tmp:
		# -O: write to file instead of stdout, name is same as on the remote
		# -L: follow redirects
		subprocess.run(['curl', '-LO', url], cwd=tmp, check=True)
		subprocess.run(['unzip', 'pkgcheck.zip'], cwd=tmp, check=True)

		install_path = os.path.split(pkgcheck)[0]
		os.makedirs(install_path, exist_ok=True)
		shutil.move(os.path.join(tmp, 'pkgcheck', 'bin', 'pkgcheck'), os.path.join(install_path, 'pkgcheck'))
		print("installed %s" % pkgcheck)


# ------- type check python scripts -------

def type_check_python_scripts() -> None:
	out = True
	out &= run_mypy('release.py', strict=True)
	out &= run_mypy(os.path.join('test', 'autotest.py'), strict=False)
	if not out:
		sys.exit(1)

def run_mypy(fn: str, *, strict: bool = True) -> bool:
	cmd = ['mypy']
	if strict:
		cmd.append('--strict')
	cmd.append(fn)
	p = subprocess.run(cmd, cwd=root, capture_output=True, text=True)
	if p.returncode != 0:
		print("ERROR: type checking failed `%s`" % ' '.join(cmd))
		return False
	out = p.stdout + p.stderr
	out = out.lower()
	if 'error' in out:
		print("ERROR: error while type checking `%s`" % ' '.join(cmd))
		return False
	if 'warn' in out:
		print("ERROR: warning while type checking `%s`" % ' '.join(cmd))
		return False
	return True


# ------- main -------

def main() -> None:
	type_check_python_scripts()
	assert_git_clean()
	run_tests()
	assert_version_and_date()
	update_file_list()
	assert_git_clean()
	build_archive()

if __name__ == '__main__':
	main()
