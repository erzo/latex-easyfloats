The documentation is in the [pdf file](./easyfloats.pdf).

On the development branch the pdf file is not always up to date, though.
See the [README](../README.md#Documentation) how to build the tex file.
